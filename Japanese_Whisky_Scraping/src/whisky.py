"""
#
# Created on 07.07.2020
#
# @author: Viktor Lavrenenko
"""

# Third-party imports
import requests
from requests.exceptions import RequestException
import bs4
from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from typing import List, Union


class Product(object):
    def __init__(self, name, percentage, price, rating=False, in_stock='Not available'):
        """
        Initializes Product instance with the following parameters
        :param name: str
            product name
        :param percentage: str
            alcohol percentage
        :param price: str
            product price   
        :param rating: str
        product rating if it's available
        :param in_stock: str
        Is the product in stock?
        """
        self.name = name
        self.percentage = percentage
        self.price = price
        self.rating = rating
        self.in_stock = in_stock

    def print(self):
        """Flexible printing function controls output"""
        print(f'Name: {self.name}')
        print(f'Percentage: {self.percentage}')
        print(f'Price: {self.price}')
        print(f'Rating: {f"{self.rating} stars" if self.rating is not None else ""}')
        print(self.in_stock)
        print('-' * 20)


class Website(object):
    def __init__(self, site_data: dict):
        """
        Initializes Website instance with the following parameters
        :param site_data:dict
        """
        self.values = list(site_data.values())
        self.name = self.values[0]  # website name
        self.url = self.values[1]  # domain name
        self.search_section = self.values[2]  # the section we're gonna scrape
        self.data = list(self.values[3].values())  # the data we will use for the search


class Scraper(object):

    def __init__(self, website: Website, headers: dict):
        """
        Initializes Scraper instance with the following parameters
        :param website: The instance of the website we're gonna scrape
        :param headers:
        """
        self.website = website
        self.headers = headers

    def get_page(self, url):
        """Makes http request, receives a response, converts this one to the BeautifulSoup object and then returns it"""
        try:
            page = requests.get(url, allow_redirects=True, headers=self.headers)
        except RequestException:
            return None
        return BeautifulSoup(page.text, 'html.parser')

    result_set = bs4.element.ResultSet
    tag = bs4.element.Tag

    def safe_get(self, page_obj: BeautifulSoup, tags: dict, many=False) -> Union[result_set, tag, None]:
        """Returns the objects selected by the specific selector"""
        if many is False:
            if len(tags) == 1:
                selected_items = page_obj.find(tags[0])
            else:
                selected_items = page_obj.find(tags[0], class_=tags[1])

        else:
            if len(tags) == 1:
                selected_items = page_obj.find_all(tags[0])
            else:
                selected_items = page_obj.find_all(tags[0], class_=tags[1])

        if selected_items is not None and len(selected_items) > 0:
            return selected_items
        return None

    def search(self) -> List[Product]:
        """Searches a given page for the specific data"""
        i = 1
        result = []
        while True:  # loop all product pages, until there are any products
            bs = self.get_page(f'{self.website.search_section}?pg={i}#productlist-filter')

            products = self.safe_get(bs, self.website.data[0], True)  # products

            if products is None or len(products) <= 0:  # break
                break

            i += 1

            for product in products:
                product_url = self.safe_get(product, self.website.data[4])  # product_url
                if product_url is not None:
                    if 'href' in product_url.attrs:
                        product_url = product_url.attrs.get('href').strip()

                percentage = self.safe_get(product, self.website.data[2])  # percentage
                if percentage is not None:
                    percentage = percentage.get_text().strip()

                price = self.safe_get(product, self.website.data[3])  # price
                if price is not None:
                    price = price.get_text().strip()

                # fetch data from the product page

                url = self.website.url + product_url
                product_page = self.get_page(url)  # product page

                product_name = self.safe_get(product_page, self.website.data[1])  # product_name
                if product_name is not None:
                    product_name = product_name.get_text().strip()

                rating = self.safe_get(product_page,
                                       self.website.data[5])  # product rating, it's often not available
                if rating is not None:
                    rating = rating.get_text().strip()

                in_stock = self.safe_get(product_page,
                                         self.website.data[6])  # here we check whether the product is in stock or not
                if in_stock is not None:
                    in_stock = in_stock.get_text().strip()
                result.append(Product(product_name, percentage, price, rating, in_stock))

        return result


if __name__ == "__main__":
    # fake user-agent
    ua = UserAgent()

    # headers
    headers = {
        'User-Agent': ua.random
    }

    # site data
    name = 'The Whisky Exchange'
    url = 'https://www.thewhiskyexchange.com'
    product_section = 'https://www.thewhiskyexchange.com/c/35/japanese-whisky'

    # selectors
    selectors = {'products': ['div', 'item'],
                 'product_name': ['h1', 'product-main__name'],
                 'product_percentage': ['span', 'meta'],
                 'price': ['span', 'price'],
                 'product_url': ['a'],
                 'rating': ['span', 'review-overview__rating star-rating star-rating--30'],
                 'in_stock': ['p', 'product-action__stock-flag']
                 }

    # site data added to the dictionary
    site_data = {
        'website_name': name,
        'website_url': url,
        'section_url': product_section,
        'selectors': selectors
    }

    website = Website(site_data)

    scraper = Scraper(website, headers)

    data = scraper.search()

    for x in data:
        x.print()
